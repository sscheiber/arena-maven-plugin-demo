package ss.arena.mavenplugins.demo;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.maven.it.VerificationException;
import org.apache.maven.it.Verifier;
import org.junit.Test;

public class CheckEncodingMojoIT {
    @Test
    public void testCountGoal() throws VerificationException, URISyntaxException, IOException {
        Verifier verifier  = new Verifier(getResourceAbsolutePath("test01"));
        verifier.executeGoal("validate");
        verifier.verifyTextInLog("my mojo");
        verifier.loadLines(verifier.getLogFileName(), "UTF-8").forEach(System.out::println);
    }
    
    private String getResourceAbsolutePath(String resource) throws URISyntaxException {
        URI uri = CheckEncodingMojoIT.class.getClassLoader().getResource(resource).toURI();
        return new File(uri).getAbsolutePath();
    }
}
