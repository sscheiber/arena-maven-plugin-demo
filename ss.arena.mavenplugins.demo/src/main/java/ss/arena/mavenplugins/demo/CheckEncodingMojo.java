package ss.arena.mavenplugins.demo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "check-encoding", defaultPhase = LifecyclePhase.VALIDATE)
public class CheckEncodingMojo extends AbstractMojo {

    @Parameter( defaultValue = "UTF-8", property = "encoding", required=true )
    private String encoding;
    
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("my mojo");
        getPluginContext().forEach((key, value) -> getLog().info(key + ": " + value));
    }
}
